using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Linq.Expressions;
using System.Threading.Tasks;
using AutoMapper;
using Core.CrossCuttingConcerns.Caching;
using Core.DataAccess;
using Core.Entities;
using Core.Entities.Concrete;
using Core.Utilities.IoC;
using Core.Utilities.Results;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;
using Microsoft.Extensions.DependencyInjection;
using System.Globalization;

namespace Core.Utilities.Business
{
    public class EntityManager<T> : EntityManager<T, T, FilterEntity>
        where T : class, IEntity, new()
    {
        public EntityManager(IEntityRepository<T> entityRepository) : base(entityRepository)
        {
        }
    }

    public class EntityManager<T, TDto> : EntityManager<T, TDto, FilterEntity>
        where T : class, IEntity, new()
        where TDto : class, new()
    {
        public EntityManager(IEntityRepository<T> entityRepository) : base(entityRepository)
        {
        }
    }

    public class EntityManager<TEntity, TDto, TFilter> : IEntityService<TEntity, TDto, TFilter>
        where TEntity : class, IEntity, new()
        where TDto : class, new()
        where TFilter : class, IFilterEntity, new()
    {
        protected readonly IEntityRepository<TEntity> _entityRepository;
        protected IMapper _mapper;

        public EntityManager(IEntityRepository<TEntity> entityRepository)
        {
            _entityRepository = entityRepository;
            _mapper = ServiceTool.ServiceProvider.GetService<IMapper>();
        }

        public async Task<IDataResult<List<TEntity>>> ListAsync()
        {
            return await ListAsync(null, null, null);
        }

        public virtual async Task<IDataResult<List<TEntity>>> ListAsync(
            Expression<Func<TEntity, bool>> predicate = null)
        {
            return await ListAsync(predicate, null, null);
        }

        public async Task<IDataResult<List<TEntity>>> ListAsync(
            Func<IQueryable<TEntity>, IIncludableQueryable<TEntity, object>> include)
        {
            return await ListAsync(null, include, null);
        }

        public async Task<IDataResult<List<TEntity>>> ListAsync(Expression<Func<TEntity, bool>> predicate,
            Func<IQueryable<TEntity>, IIncludableQueryable<TEntity, object>> include)
        {
            return await ListAsync(predicate, include, null);
        }


        public async Task<IDataResult<List<TEntity>>> ListAsync(
            Expression<Func<TEntity, bool>> predicate,
            Func<IQueryable<TEntity>, IIncludableQueryable<TEntity, object>> include,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy,
            int? index = null,
            int? count = null
        )
        {
            if (index.HasValue && count.HasValue)
            {
                index = index <= 0 ? 1 : index;
            }

            return new SuccessDataResult<List<TEntity>>(
                await _entityRepository.ListAsync(predicate, include, orderBy, index * count, count)
            );
        }

        public virtual async Task<IDataResult<PagedItems<TEntity>>> FilterAsync(
            TFilter model,
            Func<IQueryable<TEntity>, IIncludableQueryable<TEntity, object>> include = null)
        {
            var query = _entityRepository.Queryable();

            query = FilterQuery(query, model);

            int totalSize = await query.CountAsync();

            query = FilterSort(query, model.SortText);

            Pager pager = null;
            if (model.Pagination)
            {
                pager = new Pager(totalSize, model.PageNumber, model.PageSize);
                if (totalSize > 0)
                {
                    query = query.Skip((pager.CurrentPage - 1) * pager.PageSize).Take(pager.PageSize);
                }
            }

            if (include != null)
            {
                query = include(query);
            }

            return new SuccessDataResult<PagedItems<TEntity>>(
                new PagedItems<TEntity>(await query.ToListAsync(), pager)
            );
        }

        public virtual IQueryable<TEntity> FilterQuery(IQueryable<TEntity> query, TFilter model)
        {
            return query;
        }

        public virtual IQueryable<TEntity> FilterSort(IQueryable<TEntity> query, string orderTxt)
        {
            if (string.IsNullOrEmpty(orderTxt)) return query;

            var orders = orderTxt.Split(',').Select(x => x.Trim()).ToList();


            if (!orders.Any()) return query;

            var orderFirst = orders[0];
            var orderType = orderFirst.Split('.')[0];
            var propertyName = orderFirst.Split('.')[1];
            TextInfo textInfo = new CultureInfo("en-US", false).TextInfo;
            propertyName = textInfo.ToTitleCase(propertyName);
            var orderQuery = query.OrderBy(propertyName + " " + orderType.ToUpper());

            if (orders.Count() > 1)
            {
                for (int i = 1; i < orders.Count(); i++)
                {
                    var order2 = orders[i];
                    var orderType2 = order2.Split('.')[0];
                    var propertyName2 = order2.Split('.')[1];
                    TextInfo textInfo2 = new CultureInfo("en-US", false).TextInfo;
                    propertyName2 = textInfo2.ToTitleCase(propertyName2);
                    orderQuery = orderQuery.ThenBy(propertyName2 + " " + orderType2.ToUpper());
                }
            }

            return orderQuery;
        }

        public virtual async Task<IDataResult<TEntity>> GetAsync(Expression<Func<TEntity, bool>> expression,
            Func<IQueryable<TEntity>, IIncludableQueryable<TEntity, object>> include = null)
        {
            return new SuccessDataResult<TEntity>(await _entityRepository.GetAsync(expression, include));
        }

        public virtual async Task<IDataResult<TEntity>> GetByIdAsync(long id,
            Func<IQueryable<TEntity>, IIncludableQueryable<TEntity, object>> include = null)
        {
            return new SuccessDataResult<TEntity>(await _entityRepository.GetAsync(x => x.Id == id, include));
        }

        public virtual async Task<IDataResult<TEntity>> CreateAsync(TDto dto)
        {
            var model = _mapper.Map<TEntity>(dto);
            var entity = await _entityRepository.CreateAsync(model);
            return new SuccessDataResult<TEntity>(entity);
        }

        public virtual async Task<IDataResult<TEntity>> UpdateAsync(TDto dto)
        {
            var model = _mapper.Map<TEntity>(dto);
            var entry = await _entityRepository.UpdateAsync(model);
            return new SuccessDataResult<TEntity>(entry);
        }

        public virtual async Task<IResult> DeleteAsync(long id)
        {
            var entityResult = await GetByIdAsync(id);
            await _entityRepository.DeleteAsync(entityResult.Data);
            return new SuccessResult();
        }

        public async Task<IDataResult<int>> CountAsync(Expression<Func<TEntity, bool>> expression = null)
        {
            return new SuccessDataResult<int>(await _entityRepository.CountAsync(expression));
        }

        public virtual async Task<IDataResult<TDto>> GetCreateDtoAsync()
        {
            return new SuccessDataResult<TDto>(new TDto());
        }

        public virtual async Task<IDataResult<TDto>> GetUpdateDtoAsync(long id)
        {
            var entity = await _entityRepository.GetByIdAsync(id);
            var dto = _mapper.Map<TDto>(entity);
            return new SuccessDataResult<TDto>(dto);
        }
    }
}