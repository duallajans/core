using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Core.Entities;
using Core.Entities.Concrete;
using Core.Utilities.Results;
using Microsoft.EntityFrameworkCore.Query;

namespace Core.Utilities.Business
{
    public interface IEntityService<T> : IEntityService<T, T, FilterEntity>
        where T : class, IEntity, new()
    {
    }
    public interface IEntityService<T, TDto> : IEntityService<T, TDto, FilterEntity>
        where T : class, IEntity, new()
        where TDto : class, new()
    {
    }

    public interface IEntityService<T, TDto, TFilter>
        where T : class, IEntity, new()
        where TDto : class, new()
        where TFilter : class, IFilterEntity, new()
    {
        #region List

        Task<IDataResult<List<T>>> ListAsync();
        Task<IDataResult<List<T>>> ListAsync(Expression<Func<T, bool>> predicate);

        Task<IDataResult<List<T>>> ListAsync(Func<IQueryable<T>, IIncludableQueryable<T, object>> include);

        Task<IDataResult<List<T>>> ListAsync(Expression<Func<T, bool>> predicate,
            Func<IQueryable<T>, IIncludableQueryable<T, object>> include);

        Task<IDataResult<List<T>>> ListAsync(
            Expression<Func<T, bool>> predicate = null,
            Func<IQueryable<T>, IIncludableQueryable<T, object>> include = null,
            Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null,
            int? index = null,
            int? count = null
        );

        #endregion


        Task<IDataResult<PagedItems<T>>> FilterAsync(
            TFilter model,
            Func<IQueryable<T>, IIncludableQueryable<T, object>> include = null);

        Task<IDataResult<T>> GetByIdAsync(long id, Func<IQueryable<T>, IIncludableQueryable<T, object>> include = null);

        Task<IDataResult<T>> GetAsync(Expression<Func<T, bool>> expression,
            Func<IQueryable<T>, IIncludableQueryable<T, object>> include = null);

        Task<IDataResult<T>> CreateAsync(TDto model);
        Task<IDataResult<T>> UpdateAsync(TDto model);
        Task<IResult> DeleteAsync(long id);

        Task<IDataResult<int>> CountAsync(Expression<Func<T, bool>> expression = null);

        Task<IDataResult<TDto>> GetCreateDtoAsync();
        Task<IDataResult<TDto>> GetUpdateDtoAsync(long id);
    }
}