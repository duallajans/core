using System;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Core.Utilities.String
{
    public static class UniqueIdentifier
    {
        public static string Generate()
        {
            return Regex.Replace(Convert.ToBase64String(Guid.NewGuid().ToByteArray()), "[/+=]", "");
        }

        public static string GenerateStr()
        {
            StringBuilder builder = new StringBuilder();
            Enumerable
                .Range(65, 26)
                .Select(e => ((char) e).ToString())
                .Concat(Enumerable.Range(97, 26).Select(e => ((char) e).ToString()))
                .Concat(Enumerable.Range(0, 10).Select(e => e.ToString()))
                .OrderBy(e => Guid.NewGuid())
                .Take(11)
                .ToList().ForEach(e => builder.Append(e));
            return builder.ToString();
        }
    }

    public static class StringExtensions
    {
        public static string GenerateSlug(this string phrase)
        {
            string ascii = new IdnMapping().GetAscii(phrase);
            ascii.ClearCharacter(" ", "-");
            ascii.ClearCharacter("--", "-");
            return ascii;
        }

        public static void ClearCharacter(this string phrase, string sourceChr, string targetChr)
        {
            while (phrase.Contains(sourceChr))
                phrase = phrase.Replace(sourceChr, targetChr);
        }

        public static string RemoveDiacritics(this string text)
        {
            return new string(text.Normalize((NormalizationForm) 2)
                    .Where<char>((Func<char, bool>) (c =>
                        CharUnicodeInfo.GetUnicodeCategory(c) != UnicodeCategory.NonSpacingMark)).ToArray<char>())
                .Normalize((NormalizationForm) 1);
        }
    }
}