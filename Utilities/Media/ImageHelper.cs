﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Core.Utilities.String;
using ImageMagick;

namespace Core.Utilities.Media
{
    public static class ImageHelper
    {
        public static string SaveImage(string base64File, string slug = "image")
        {
            var extension = "png";
            var fileName = slug.GenerateSlug() + "_" + UniqueIdentifier.GenerateStr() + "." + extension;
            var folder = Directory.GetCurrentDirectory() + "/wwwroot/upload/";
            var returnPath = "/upload/" + fileName;
            string path = folder + fileName;
            if (!Directory.Exists(folder))
            {
                Directory.CreateDirectory(folder);
            }

            var base64 = Convert.FromBase64String(base64File);
            using FileStream fileStream = File.Create(path);
            fileStream.Write(base64);
            fileStream.Flush();
            return returnPath;
        }

        public static string SaveImage(this IFormFile file, string slug = "image")
        {
            var extension = file.FileName.Split('.').Last();
            var fileName = slug.GenerateSlug() + "-" + UniqueIdentifier.GenerateStr() + "." + extension;
            var folder = Directory.GetCurrentDirectory() + "/wwwroot/upload/";
            var returnPath = "/upload/" + fileName;
            string path = folder + fileName;
            if (!Directory.Exists(folder))
            {
                Directory.CreateDirectory(folder);
            }

            using (FileStream fileStream = File.Create(path))
            {
                file.CopyTo(fileStream);
                fileStream.Flush();
            }

            return returnPath;
        }
    }
}