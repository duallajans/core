using System;

namespace Core.Utilities.Exceptions
{
    public class AppException : Exception
    {
        public AppException(string message) : base(message)
        {
        }
    }

    public class DisplayException : AppException
    {
        public DisplayException(string message) : base(message)
        {
        }
    }
}