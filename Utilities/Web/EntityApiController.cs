using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Entities;
using Core.Entities.Concrete;
using Core.Utilities.Business;
using Microsoft.AspNetCore.Mvc;

namespace Core.Utilities.Web
{
    public abstract class EntityApiController<T> : EntityApiController<T, T, FilterEntity>
        where T : class, IEntity, new()
    {
        public EntityApiController(IEntityService<T> service) : base(service)
        {
        }
    }

    public abstract class EntityApiController<T, TDto> : EntityApiController<T, TDto, FilterEntity>
        where T : class, IEntity, new()
        where TDto : class, new()
    {
        public EntityApiController(IEntityService<T, TDto> service) : base(service)
        {
        }
    }

    public abstract class EntityApiController<T, TDto, TFilter> : ApiController
        where T : class, IEntity, new()
        where TDto : class, new()
        where TFilter : class, IFilterEntity, new()
    {
        protected readonly IEntityService<T, TDto, TFilter> _entityService;

        public EntityApiController(IEntityService<T, TDto, TFilter> service)
        {
            _entityService = service;
        }

        [HttpPost("filter")]
        public virtual async Task<IActionResult> Filter(
            [FromBody] TFilter filterEntity
        )
        {
            return Ok(await _entityService.FilterAsync(filterEntity));
        }
        

        [HttpGet("list")]
        public virtual async Task<IActionResult> List()
        {
            return Ok(await _entityService.ListAsync());
        }

        [HttpGet("get/{id}")]
        public virtual async Task<IActionResult> Get(long id)
        {
            return Ok(await _entityService.GetByIdAsync(id));
        }

        [HttpPost("create")]
        public virtual async Task<IActionResult> Create([FromBody] TDto model)
        {
            return Ok(await _entityService.CreateAsync(model));
        }


        [HttpPut("update")]
        public virtual async Task<IActionResult> Update([FromBody] TDto model)
        {
            return Ok(await _entityService.UpdateAsync(model));
        }

        [HttpDelete("delete/{id}")]
        public virtual async Task<IActionResult> Delete(long id)
        {
            return Ok(await _entityService.DeleteAsync(id));
        }

        [HttpGet("getDto")]
        [HttpGet("getDto/{id}")]
        public virtual async Task<IActionResult> GetDto(long? id = null)
        {
            return id.HasValue
                ? Ok(await _entityService.GetUpdateDtoAsync(id.Value))
                : Ok(await _entityService.GetCreateDtoAsync());
        }
    }
}