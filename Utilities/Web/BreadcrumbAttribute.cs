using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace Core.Utilities.Web
{
    public class BreadcrumbAttribute : ActionFilterAttribute
    {
        private readonly string _pageTitle;
        private readonly string _pageSubTitle;

        public BreadcrumbAttribute(string pageTitle = "", string pageSubTitle = "")
        {
            _pageTitle = pageTitle;
            _pageSubTitle = pageSubTitle;
        }

        public override void OnActionExecuting(ActionExecutingContext context)
        {
            base.OnActionExecuting(context);
            base.OnActionExecuting(context);
            if (!(context.Controller is Controller controller))
                return;
            if (!string.IsNullOrEmpty(_pageTitle))
            {
                controller.ViewBag.PageTitle = _pageTitle;
            }

            if (!string.IsNullOrEmpty(_pageSubTitle))
            {
                controller.ViewBag.PageSubTitle = _pageSubTitle;
            }
        }
    }
}