using Core.Utilities.Results;
using Core.Utilities.Web.Models;
using Microsoft.AspNetCore.Mvc;

namespace Core.Utilities.Web
{
    public class ApiController : Controller
    {
        protected IActionResult Ok(IResult result)
        {
            if(result.Success) return base.Ok(new ApiResult(result));
            return BadRequest(new ApiResult(result));
        }

        protected IActionResult Ok<T>(IDataResult<T> result)
        {
            if(result.Success) return base.Ok(new ApiResult<T>(result));
            return BadRequest(new ApiResult<T>(result));
        }
    }
}