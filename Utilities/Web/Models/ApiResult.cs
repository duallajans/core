using Core.Utilities.Results;

namespace Core.Utilities.Web.Models
{
    public class ApiResult
    {
        public ApiResult(IResult result)
        {
            Success = result.Success;
            Message = result.Message;
        }
        
        public bool Success { get; set; }
        public string Message { get; set; }
        
    }
    public class ApiResult<T> : ApiResult
    {
        public T Data { get; }

        public ApiResult(IDataResult<T> result) : base(result)
        {
            Data = result.Data;
        }
    }
}