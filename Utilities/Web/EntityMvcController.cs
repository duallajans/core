using System.Threading.Tasks;
using AutoMapper;
using Core.Entities;
using Core.Entities.Concrete;
using Core.Extensions;
using Core.Utilities.Business;
using Core.Utilities.IoC;
using Core.Utilities.Results;
using Core.Utilities.Web.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;

namespace Core.Utilities.Web
{
    public class EntityMvcController<T, TDto> : AppControllerBase
        where T : class, IEntity, new()
        where TDto : class, new()
    {
        protected IEntityService<T, TDto> _crudService;

        public EntityMvcController(IEntityService<T, TDto> service)
        {
            _crudService = service;
        }

        protected string ListViewName { get; set; }
        protected string CreateViewName { get; set; }
        protected string UpdateViewName { get; set; }

        public virtual IActionResult Index() => RedirectToAction("List");

        public virtual async Task<IActionResult> List()
        {
            var result = await _crudService.ListAsync();
            return View(ListViewName ?? "List", result.Data);
        }


        public virtual async Task<IActionResult> Create()
        {
            var result = await _crudService.GetCreateDtoAsync();
            return View(CreateViewName ?? "CreateOrUpdate", result.Data);
        }

        [HttpPost]
        public virtual async Task<IActionResult> Create([FromForm] TDto model)
        {
            var result = await _crudService.CreateAsync(model);
            if (!result.Success) return ErrorRequest(model, result);
            var url = Url.Action("Update", new {id = result.Data.Id});
            if (!string.IsNullOrEmpty(Request.Form["ReturnUrl"]))
            {
                url = Request.Form["ReturnUrl"];
            }
            return SuccessRequest(url);
        }


        public virtual async Task<IActionResult> Update(long id)
        {
            var result = await _crudService.GetUpdateDtoAsync(id);
            return View(UpdateViewName ?? "CreateOrUpdate", result.Data);
        }

        [HttpPost]
        public virtual async Task<IActionResult> Update(TDto model)
        {
            var result = await _crudService.UpdateAsync(model);
            if (!result.Success) return ErrorRequest(model, result);
            var url = Url.Action("Update", new {id = result.Data.Id});
            if (!string.IsNullOrEmpty(Request.Form["ReturnUrl"]))
            {
                url = Request.Form["ReturnUrl"];
            }
            return SuccessRequest(url);
        }

        public virtual async Task<IActionResult> Delete(long id)
        {
            await _crudService.DeleteAsync(id);
            return SuccessRequest(Url.Action("List"));
        }


        protected IActionResult ErrorRequest(TDto model, IResult result)
        {
            if (Request.IsAjaxRequest())
            {
                return BadRequest(new {Message = result.Message});
            }

            ViewBag.Result = result;
            return View(model);
        }
    }
}