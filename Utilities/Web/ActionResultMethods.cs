using Core.Extensions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Core.Utilities.Web
{
    public class AppControllerBase : Controller
    {
        protected IActionResult SuccessRequest(string redirectPath)
        {
            if (Request.IsAjaxRequest())
            {
                return Ok(new {Redirect = redirectPath});
            }

            return Redirect(redirectPath);
        }
    }
}