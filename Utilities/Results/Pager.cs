using System;
using System.Collections.Generic;
using System.Linq;

namespace Core.Utilities.Results
{
    public class PagedItems<T>
    {
        public PagedItems(List<T> data, Pager pager)
        {
            List = data;
            if (pager == null) return;
            PageSize = pager.PageSize;
            Page = pager.CurrentPage;
            TotalPages = pager.TotalPages;
            TotalRecords = pager.TotalItems;
        }

        public int PageSize { get; set; }
        public int Page { get; set; }
        public int TotalPages { get; set; }
        public int TotalRecords { get; set; }
        public List<T> List { get; set; }
    }

    public class Pager
    {
        public Pager(int totalItems, int currentPage = 1, int pageSize = 10, int maxPages = 6)
        {
            int num1 = (int)Math.Ceiling((Decimal)totalItems / (Decimal)pageSize);
            if (currentPage < 1)
                currentPage = 1;
            else if (currentPage > num1)
                currentPage = num1;
            int start;
            int num2;
            if (num1 <= maxPages)
            {
                start = 1;
                num2 = num1;
            }
            else
            {
                int num3 = (int)Math.Floor((Decimal)maxPages / new Decimal(2));
                int num4 = (int)Math.Ceiling((Decimal)maxPages / new Decimal(2)) - 1;
                if (currentPage <= num3)
                {
                    start = 1;
                    num2 = maxPages;
                }
                else if (currentPage + num4 >= num1)
                {
                    start = num1 - maxPages + 1;
                    num2 = num1;
                }
                else
                {
                    start = currentPage - num3;
                    num2 = currentPage + num4;
                }
            }

            int num5 = (currentPage - 1) * pageSize;
            int num6 = Math.Min(num5 + pageSize - 1, totalItems - 1);
            IEnumerable<int> ints = Enumerable.Range(start, num2 + 1 - start);
            this.TotalItems = totalItems;
            this.CurrentPage = currentPage;
            this.PageSize = pageSize;
            this.TotalPages = num1;
            this.StartPage = start;
            this.EndPage = num2;
            this.StartIndex = num5;
            this.EndIndex = num6;
            this.Pages = ints;
        }

        public int TotalItems { get; private set; }

        public int CurrentPage { get; private set; }

        public int PageSize { get; private set; }

        public int TotalPages { get; private set; }

        public int StartPage { get; private set; }

        public int EndPage { get; private set; }

        public int StartIndex { get; private set; }

        public int EndIndex { get; private set; }

        public IEnumerable<int> Pages { get; private set; }

        public bool IsFirstPage
        {
            get { return this.CurrentPage == 1; }
        }

        public bool IsCurrentPage(int page)
        {
            return this.CurrentPage == page;
        }

        public bool IsLastPage
        {
            get { return this.CurrentPage == this.EndPage; }
        }
    }
}