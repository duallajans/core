namespace Core.CrossCuttingConcerns.Logging.Log4Net.Loggers
{
    public class ConsoleLogger : LoggerServiceBase
    {
        public ConsoleLogger() : base("ConsoleLogger")
        {
        }
    }
}