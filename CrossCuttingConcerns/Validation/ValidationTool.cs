﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core.Entities;
using FluentValidation;
using FluentValidation.Results;

namespace Core.CrossCuttingConcerns.Validation
{
    public static class ValidationTool
    {
        public static void Validate(IValidator validator,object entity)
        {
            var result = validator.Validate(entity);
            if (!result.IsValid)
            {
                IEnumerable<string> values = result.Errors.Select(x => x.PropertyName + ", " + x.ErrorMessage + Environment.NewLine);
                var str = string.Join(string.Empty, values);
                throw new ValidationException(result.Errors);
            }
        }
    }
}
