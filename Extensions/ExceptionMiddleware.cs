﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Core.CrossCuttingConcerns.Logging.Log4Net.Loggers;
using Core.Utilities.Exceptions;
using FluentValidation;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Core.Extensions
{
    public class ExceptionMiddleware
    {
        private RequestDelegate _next;
        private FileLogger _logger;

        public ExceptionMiddleware(RequestDelegate next, FileLogger logger)
        {
            _next = next;
            _logger = logger;
        }

        public async Task InvokeAsync(HttpContext httpContext)
        {
            try
            {
                await _next(httpContext);
            }
            catch (Exception e)
            {
                await HandleExceptionAsync(httpContext, e);
            }
        }

        private Task HandleExceptionAsync(HttpContext httpContext, Exception e)
        {
            var exceptionMessage = "Internal server error.";
            if (e is ValidationException || e is DisplayException)
            {
                exceptionMessage = e.Message;
            }
            else
            {
	            var st = new StackTrace(e, true);
	            var frame = st.GetFrame(0);
	            int line = 0;
	            string file = "";
	            if (frame != null)
	            {
		            line = frame.GetFileLineNumber();
		            file = frame.GetFileName();
	            }

	            _logger.Info(new ErrorDetails
	            {
		            Message = e.Message,
		            Source = $"{file}:{line}"
	            }.ToString());
            }

            if (!httpContext.Request.IsAjaxRequest())
            {
                return httpContext.Response.WriteAsync($@"
<!DOCTYPE html>
<html lang=""en"">
	<head>
		<meta charset=""utf-8"" />
		<title>404 | Tuprag</title>
		<meta name=""description"" content="""" />
		<meta name=""viewport"" content=""width=device-width, initial-scale=1, shrink-to-fit=no"" />
		<link rel=""canonical"" href=""https://keenthemes.com/metronic"" />
		<link rel=""stylesheet"" href=""https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700"" />
		<link href=""/assets/css/pages/error/error-6.css"" rel=""stylesheet"" type=""text/css"" />
		<link href=""/assets/plugins/global/plugins.bundle.css"" rel=""stylesheet"" type=""text/css"" />
		<link href=""/assets/plugins/custom/prismjs/prismjs.bundle.css"" rel=""stylesheet"" type=""text/css"" />
		<link href=""/assets/css/style.bundle.css"" rel=""stylesheet"" type=""text/css"" />
		<link href=""/assets/css/themes/layout/header/base/light.css"" rel=""stylesheet"" type=""text/css"" />
		<link href=""/assets/css/themes/layout/header/menu/light.css"" rel=""stylesheet"" type=""text/css"" />
		<link href=""/assets/css/themes/layout/brand/dark.css"" rel=""stylesheet"" type=""text/css"" />
		<link href=""/assets/css/themes/layout/aside/dark.css"" rel=""stylesheet"" type=""text/css"" />
		<link rel=""shortcut icon"" href=""/assets/media/logos/favicon.ico"" />
	</head>
	<body id=""kt_body"" class=""header-fixed header-mobile-fixed subheader-enabled subheader-fixed aside-enabled aside-fixed aside-minimize-hoverable page-loading"">
		<div class=""d-flex flex-column flex-root"">
			<div class=""error error-6 d-flex flex-row-fluid bgi-size-cover bgi-position-center"" style=""background-image: url(/assets/media/error/bg6.jpg);"">
				<div class=""d-flex flex-column flex-row-fluid text-center"">
					<h1 class=""error-title font-weight-boldest text-white mb-12"" style=""margin-top: 12rem;"">404</h1>
					<p class=""display-4 font-weight-bold text-white"">{exceptionMessage}</p>
				</div>
			</div>
		</div>
		<script>var HOST_URL = ""/"";</script>
		<script>var KTAppSettings = {{ ""breakpoints"": {{ ""sm"": 576, ""md"": 768, ""lg"": 992, ""xl"": 1200, ""xxl"": 1400 }}, ""colors"": {{ ""theme"": {{ ""base"": {{ ""white"": ""#ffffff"", ""primary"": ""#3699FF"", ""secondary"": ""#E5EAEE"", ""success"": ""#1BC5BD"", ""info"": ""#8950FC"", ""warning"": ""#FFA800"", ""danger"": ""#F64E60"", ""light"": ""#E4E6EF"", ""dark"": ""#181C32"" }}, ""light"": {{ ""white"": ""#ffffff"", ""primary"": ""#E1F0FF"", ""secondary"": ""#EBEDF3"", ""success"": ""#C9F7F5"", ""info"": ""#EEE5FF"", ""warning"": ""#FFF4DE"", ""danger"": ""#FFE2E5"", ""light"": ""#F3F6F9"", ""dark"": ""#D6D6E0"" }}, ""inverse"": {{ ""white"": ""#ffffff"", ""primary"": ""#ffffff"", ""secondary"": ""#3F4254"", ""success"": ""#ffffff"", ""info"": ""#ffffff"", ""warning"": ""#ffffff"", ""danger"": ""#ffffff"", ""light"": ""#464E5F"", ""dark"": ""#ffffff"" }} }}, ""gray"": {{ ""gray-100"": ""#F3F6F9"", ""gray-200"": ""#EBEDF3"", ""gray-300"": ""#E4E6EF"", ""gray-400"": ""#D1D3E0"", ""gray-500"": ""#B5B5C3"", ""gray-600"": ""#7E8299"", ""gray-700"": ""#5E6278"", ""gray-800"": ""#3F4254"", ""gray-900"": ""#181C32"" }} }}, ""font-family"": ""Poppins"" }};</script>
		<script src=""/assets/plugins/global/plugins.bundle.js""></script>
		<script src=""/assets/plugins/custom/prismjs/prismjs.bundle.js""></script>
		<script src=""/assets/js/scripts.bundle.js""></script>
	</body>
</html>");
            }

            httpContext.Response.ContentType = "application/json";
            httpContext.Response.StatusCode = (int) HttpStatusCode.InternalServerError;

            return httpContext.Response.WriteAsync(new ErrorDetails
            {
                StatusCode = httpContext.Response.StatusCode,
                Message = exceptionMessage,
            }.ToString());
        }
    }
}