using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using Core.Entities;
using Core.Entities.Concrete;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.ViewEngines;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.Extensions.Primitives;

namespace Core.Extensions
{
    public static class ControllerExtensions
    {
        public static object Parse(string valueToConvert, Type dataType)
        {
            return TypeDescriptor.GetConverter(dataType)
                .ConvertFromString(null, CultureInfo.InvariantCulture, valueToConvert);
        }

        public static bool IsAjaxRequest(this HttpRequest request)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));
            if (request.Headers["X-Requested-With"] == "XMLHttpRequest")
                return true;
            if (request.Headers != null)
                return request.Headers["X-Requested-With"] == "XMLHttpRequest";
            return false;
        }

        public static string RenderView(
            this Controller controller,
            string viewName,
            object model,
            bool partial = false)
        {
            controller.ViewBag.RenderPartial = partial;
            if (string.IsNullOrEmpty(viewName))
                viewName = controller.ControllerContext.ActionDescriptor.ActionName;
            controller.ViewData.Model = model;
            using (StringWriter stringWriter = new StringWriter())
            {
                IViewEngine service =
                    controller.HttpContext.RequestServices.GetService(typeof(ICompositeViewEngine)) as
                        ICompositeViewEngine;
                if (service != null)
                {
                    ViewEngineResult view = service.FindView(controller.ControllerContext, viewName, !partial);
                    if (!view.Success)
                        return "A view with the name " + viewName + " could not be found";
                    ViewContext viewContext = new ViewContext(controller.ControllerContext, view.View,
                        controller.ViewData, controller.TempData, stringWriter, new HtmlHelperOptions());
                    controller.ViewBag.RenderPartial = partial;

                    view.View.RenderAsync(viewContext).Wait();
                }

                return stringWriter.GetStringBuilder().ToString();
            }
        }
    }
}