﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Core.Entities;
using Core.Entities.Concrete;
using Core.Utilities.Results;
using Microsoft.EntityFrameworkCore.Query;

namespace Core.DataAccess
{
    public interface IEntityRepository<TEntity> where TEntity : class, IEntity, new()
    {
        #region List

        Task<List<TEntity>> ListAsync();
        Task<List<TEntity>> ListAsync(Expression<Func<TEntity, bool>> predicate);
        Task<List<TEntity>> ListAsync(Func<IQueryable<TEntity>, IIncludableQueryable<TEntity, object>> include);

        Task<List<TEntity>> ListAsync(Expression<Func<TEntity, bool>> predicate,
            Func<IQueryable<TEntity>, IIncludableQueryable<TEntity, object>> include);

        Task<List<TEntity>> ListAsync(
            Expression<Func<TEntity, bool>> predicate = null,
            Func<IQueryable<TEntity>, IIncludableQueryable<TEntity, object>> include = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            int? skip = null, int? take = null);

        #endregion

        #region Get

        Task<TEntity> GetAsync(
            Expression<Func<TEntity, bool>> predicate = null,
            Func<IQueryable<TEntity>, IIncludableQueryable<TEntity, object>> include = null
        );

        Task<TEntity> GetByIdAsync(
            long id,
            Func<IQueryable<TEntity>, IIncludableQueryable<TEntity, object>> include = null
        );

        #endregion

        Task<TEntity> CreateAsync(TEntity entity);
        Task<TEntity> UpdateAsync(TEntity entity);
        Task DeleteAsync(TEntity entity);
        Task DeleteAsync(long id);
        Task HardDelete(TEntity entity);
        Task HardDelete(long id);

        IQueryable<TEntity> Queryable();
        Task<int> CountAsync();
        Task<int> CountAsync(Expression<Func<TEntity, bool>> filter);
    }
}