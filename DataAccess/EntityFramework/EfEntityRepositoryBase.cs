﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Core.Entities;
using Core.Entities.Concrete;
using Core.Utilities.Results;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;

namespace Core.DataAccess.EntityFramework
{
    public abstract class EfEntityRepositoryBase<TEntity, TContext> : IEntityRepository<TEntity>
        where TEntity : class, IEntity, new()
        where TContext : DbContext, new()
    {
        #region List

        public async Task<List<TEntity>> ListAsync()
        {
            return await ListAsync(null, null, null, null, null);
        }

        public async Task<List<TEntity>> ListAsync(Expression<Func<TEntity, bool>> predicate)
        {
            return await ListAsync(predicate, null, null, null, null);
        }

        public async Task<List<TEntity>> ListAsync(
            Func<IQueryable<TEntity>, IIncludableQueryable<TEntity, object>> include)
        {
            return await ListAsync(null, include, null, null, null);
        }

        public async Task<List<TEntity>> ListAsync(Expression<Func<TEntity, bool>> predicate,
            Func<IQueryable<TEntity>, IIncludableQueryable<TEntity, object>> include)
        {
            return await ListAsync(predicate, include);
        }

        public async Task<List<TEntity>> ListAsync(
            Expression<Func<TEntity, bool>> predicate = null,
            Func<IQueryable<TEntity>, IIncludableQueryable<TEntity, object>> include = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null, int? skip = null,
            int? take = null
        )
        {
            IQueryable<TEntity> query = Queryable(predicate, include);

            if (BaseCondition != null) query = query.Where(BaseCondition);

            if (orderBy != null)
            {
                query = orderBy(query);
            }

            if (skip.HasValue)
            {
                query = query.Skip(skip.Value);
            }

            if (take.HasValue)
            {
                query = query.Take(take.Value);
            }

            return await query.ToListAsync();
        }

        #endregion

        #region Get

        public async Task<TEntity> GetAsync(Expression<Func<TEntity, bool>> predicate = null,
            Func<IQueryable<TEntity>, IIncludableQueryable<TEntity, object>> include = null)
        {
            return await Queryable(predicate, include).FirstOrDefaultAsync();
        }

        public async Task<TEntity> GetByIdAsync(long id,
            Func<IQueryable<TEntity>, IIncludableQueryable<TEntity, object>> include = null)
        {
            return await Queryable(x => x.Id == id, include).FirstOrDefaultAsync();
        }

        #endregion

        #region Create

        public async Task<TEntity> CreateAsync(TEntity entity)
        {
            await using var context = new TContext();
            if (entity is BaseEntity baseEntity)
            {
                var now = DateTime.Now;
                baseEntity.UpdatedDate = now;
                baseEntity.CreatedDate = now;
            }

            var addedEntity = await context.Set<TEntity>().AddAsync(entity);
            await context.SaveChangesAsync();
            return addedEntity.Entity;
        }

        #endregion

        #region Update

        public async Task<TEntity> UpdateAsync(TEntity model)
        {
            await using var context = new TContext();
            if (model is BaseEntity baseEntity)
            {
                baseEntity.UpdatedDate = DateTime.Now;
            }

            var entity = await GetByIdAsync(model.Id);
            context.Entry(entity).CurrentValues.SetValues(model);
            context.Entry(entity).State = EntityState.Modified;
            await context.SaveChangesAsync();
            return entity;
        }

        #endregion

        #region Delete

        public virtual async Task DeleteAsync(TEntity entity)
        {
            await using var context = new TContext();
            entity.DeletedDate = DateTime.Now;
            var deletedEntity = context.Entry(entity);
            deletedEntity.State = EntityState.Modified;
            await context.SaveChangesAsync();
        }

        public async Task DeleteAsync(long id)
        {
            await DeleteAsync(await GetByIdAsync(id));
        }

        public async Task HardDelete(TEntity entity)
        {
            await using var context = new TContext();
            var deletedEntity = context.Entry(entity);
            deletedEntity.State = EntityState.Deleted;
            await context.SaveChangesAsync();
        }

        public async Task HardDelete(long id)
        {
            await HardDelete(await GetByIdAsync(id));
        }

        #endregion

        #region Count

        public async Task<int> CountAsync()
        {
            return await CountAsync(null);
        }

        public async Task<int> CountAsync(Expression<Func<TEntity, bool>> filter)
        {
            await using var context = new TContext();
            return filter == null
                ? await context.Set<TEntity>().CountAsync(BaseCondition)
                : await context.Set<TEntity>().Where(BaseCondition).CountAsync(filter);
        }

        #endregion

        public IQueryable<TEntity> Queryable()
        {
            return Queryable(null);
        }

        protected virtual Expression<Func<TEntity, bool>> BaseCondition => x => x.DeletedDate == null;
        protected virtual Func<IQueryable<TEntity>, IIncludableQueryable<TEntity, object>> BaseInclude => null;

        private IQueryable<TEntity> Queryable(Expression<Func<TEntity, bool>> predicate = null,
            Func<IQueryable<TEntity>, IIncludableQueryable<TEntity, object>> include = null)
        {
            var context = new TContext();
            var query = context.Set<TEntity>().AsQueryable().Where(BaseCondition);


            if (include != null)
            {
                query = include(query);
            }

            if (predicate != null)
            {
                query = query.Where(predicate);
            }

            return query;
        }
    }
}