﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Entities
{
    public interface IEntity
    {
        long Id { get; set; }
        DateTime? DeletedDate { get; set; }
    }
}
