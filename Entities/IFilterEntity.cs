namespace Core.Entities
{
    public interface IFilterEntity
    {
        string SearchText { get; set; }

        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public string SortText { get; set; }
        public bool Pagination { get; set; }
    }
}