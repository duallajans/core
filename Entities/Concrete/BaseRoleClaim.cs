namespace Core.Entities.Concrete
{
    public class BaseRoleClaim : Entity
    {
        public BaseRole Role { get; set; }
        public long RoleId { get; set; }

        public BaseClaim Claim { get; set; }
        public long ClaimId { get; set; }
    }
}