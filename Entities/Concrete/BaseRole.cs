﻿using System;
using System.Collections.Generic;
using System.Text;
using Core.Entities;

namespace Core.Entities.Concrete
{
    public class BaseRole : Entity
    {
        public string Name { get; set; }
    }
}