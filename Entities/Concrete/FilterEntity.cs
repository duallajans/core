namespace Core.Entities.Concrete
{
    public class FilterEntity : IFilterEntity
    {
        public string SearchText { get; set; }

        public int PageNumber { get; set; } = 1;
        public int PageSize { get; set; } = 10;
        public string SortText { get; set; } = "desc.Id";
        public bool Pagination { get; set; } = true;
    }
}