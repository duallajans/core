using System;

namespace Core.Entities.Concrete
{
    public class Entity : IEntity
    {
        public long Id { get; set; }
        public DateTime? DeletedDate { get; set; }
    }
}