using System;

namespace Core.Entities.Concrete
{
    public class BaseEntity : Entity
    {
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
    }
}