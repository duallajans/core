namespace Core.Entities
{
    public interface IUser : IEntity
    {
        string Email { get; set; }
        string FirstName { get; set; }
        string LastName { get; set; }
    }
}